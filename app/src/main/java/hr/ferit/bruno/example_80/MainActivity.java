package hr.ferit.bruno.example_80;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    @BindView(R.id.ivFirst) ImageView ivFirst;
    @BindView(R.id.ivSecond) ImageView ivSecond;
    @BindView(R.id.ivThird) ImageView ivThird;

    String mSecondImageURL = "https://cdn.pixabay.com/photo/2015/06/03/14/27/ladybug-796483_960_720.jpg";
    int mFirstImageOnDisk = R.drawable.first;
    int mThirdImageOnDisk = R.drawable.third;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadImages();
    }

    private void loadImages() {
        Context ctx = getApplicationContext();
        Picasso.with(ctx).load(mFirstImageOnDisk).fit().into(ivFirst);
        Picasso.with(ctx).load(mSecondImageURL).fit().into(ivSecond);
        Picasso.with(ctx).load(mThirdImageOnDisk).fit().into(ivThird);
    }
}
